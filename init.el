;; Loads the config.org babel file, originally based on
;; http://orgmode.org/worg/org-contrib/babel/intro.html

;; Helper function
(defun dotemacs-joindirs (root &rest dirs)
  "Joins a series of directories together, like Python's os.path.join,
  (dotemacs-joindirs \"/tmp\" \"a\" \"b\" \"c\") => /tmp/a/b/c"

  (if (not dirs)
      root ; No more dirs to join, no more recursing
    (apply 'dotemacs-joindirs
           (expand-file-name (car dirs) root)
           (cdr dirs))))


;; Determine the directory containing this file
(setq dotfiles-dir (file-name-directory (or load-file-name
                                            (buffer-file-name))))


;; emacs 23 requires newer version of org-mode than default, e.g:
;; $ git clone git://orgmode.org/org-mode.git ext/org-mode/
;; A checkout of a tag like release_7.5 definitely works in emacs 23
(if (< emacs-major-version 24)
    (add-to-list 'load-path (dotemacs-joindirs dotfiles-dir "ext" "org-mode" "lisp")))


;; Load up org-mode and org-babel
(require 'org-install)
(require 'ob-tangle)


;; Load up the main file
(org-babel-load-file (dotemacs-joindirs dotfiles-dir "config.org"))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes (quote ("fc5fcb6f1f1c1bc01305694c59a1a861b008c534cae8d0e48e4d5e81ad718bc6" default)))
 '(jabber-alert-presence-hooks nil)
 '(jabber-chat-buffer-show-avatar nil)
 '(jabber-chat-prompt-foreign ((t (:foreground "orange" :weight bold))))
 '(jabber-chat-prompt-local ((t (:foreground "peru" :weight bold))))
 '(jabber-roster-line-format " %c %-25n %u %-8s  %S")
 '(jabber-roster-user-online ((t (:foreground "burlywood" :slant normal :weight bold))))
 '(jabber-title-large ((t (:inherit variable-pitch :weight bold :height 1.0 :width ultra-expanded))))
 '(jabber-title-medium ((t (:inherit variable-pitch :weight semi-bold :height 1.0 :width expanded))))
 '(jabber-title-small ((t (:inherit variable-pitch :weight normal :height 1.0 :width semi-expanded)))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
